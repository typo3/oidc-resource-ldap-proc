<?php

namespace IMATHUZH\OidcResourceLdapProcessor\EventListeners;

use Causal\IgLdapSsoAuth\Domain\Repository\ConfigurationRepository;
use Causal\IgLdapSsoAuth\Library\Configuration;
use Causal\IgLdapSsoAuth\Library\Ldap;
use IMATHUZH\OidcClient\Event\ModifyResourceOwnerEvent;
use IMATHUZH\OidcResourceLdapProcessor\PropertyMapper;
use IMATHUZH\OidcResourceLdapProcessor\Utility\Constants;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class LdapProcessor implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    public function __invoke(ModifyResourceOwnerEvent $event): void
    {
        $config = $event->getProcessorConfig(Constants::PROCESSOR_ID);
        // configuration entries:
        // - enabled: 1 if the processor should be used
        // - servers: a list of uid of ldap servers from tx_igldapssoauth_config
        // - filter: filter string for ldap query
        // - mapping: mapping rules of ldap to oidc resource
        if (!$config['enabled']) return;

        $resource = $event->getResourceOwner();
        $filter = PropertyMapper::evaluateTemplate(
            $config['filter'] ?? '',
            $resource->getClaims()
        );
        $mappingRules = $config['mapping'] ?? '';
        $foundFields = preg_match_all('/<(\w+)>/', $mappingRules, $ldapFields, PREG_PATTERN_ORDER);
        // Stop if nothing to map or no filter is provided
        if (!$filter || !$mappingRules || !$foundFields) {
            return;
        }

        // Get a user
        $this->logger->debug("Making a query", [
            'filter' => $filter,
            'fields' => $ldapFields[1]
        ]);
        $serverList = GeneralUtility::intExplode(',', $config['servers'], true);
        $ldapEntry = $this->findEntry($filter, $ldapFields[1], $serverList, $event->getLoginType());

        if (!$ldapEntry) {
            $this->logger->warning("No user matching $filter");
        } else {
            $this->logger->debug("Found a user matching $filter", $ldapEntry);
            $mapper = PropertyMapper::fromUserText($mappingRules);
            $resource->setClaims(array_merge(
                $resource->getClaims(),
                $mapper->eval($ldapEntry)
            ));
            $this->logger->debug("Resource updated with LDAP data", $resource->getClaims());
        }
    }

    protected function findEntry(string $filter, array $fields, array $serverUids, string $loginType): ?array
    {
        $configurationRepository = GeneralUtility::makeInstance(ConfigurationRepository::class);
        $ldapConfigurations = $configurationRepository->findAll();

        // Loop on each configuration to find the user
        foreach ($ldapConfigurations as $configuration) {

            if (!in_array($configuration->getUid(), $serverUids)) {
                continue;
            }
            Configuration::initialize($loginType, $configuration);
            // Start by connecting to the designated LDAP/AD server
            $ldapInstance = Ldap::getInstance();
            if (!$ldapInstance->connect(Configuration::getLdapConfiguration())) {
                continue;
            }
            $ldapClientConfig = Configuration::getFrontendConfiguration();

            $response = $ldapInstance->search(
                $ldapClientConfig['users']['basedn'],
                $filter,
                $fields,
                true
            );
            $ldapInstance->disconnect();

            /** @todo this should be improved - why are the values arrays? */
            if ($response) {
                return array_map(
                    function ($value) { return is_array($value) ? $value[0] : $value; },
                    $response
                );
            }

            // Consider that fetching no users from LDAP is an error
            $this->logger->error(sprintf(
                'No entry matching %s found for configuration record %s', $filter, $configuration->getUid()
            ));
        }

        $this->logger->error(sprintf(
            'None of LDAP servers has a user matching %s', $filter
        ));
        return null;
    }
}