<?php

namespace IMATHUZH\OidcResourceLdapProcessor;

use TYPO3\CMS\Core\Utility\GeneralUtility;

class PropertyMapper
{
    const COMMENT_TOKEN = '#';

    protected array $rules;

    public function __construct(array $rules)
    {
        $this->rules = $rules;
    }

    static public function fromUserText(string $text): self
    {
        $rules = [];
        foreach (GeneralUtility::trimExplode("\n", $text, true) as $line) {
            // Test if a comment
            if ($line[0] !== self::COMMENT_TOKEN) {
                $parts = explode('=', $line, 2);
                // the property is already trimmed from the left side
                $property = rtrim($parts[0] ?? '');
                if ($property) {
                    $rules[$property] = GeneralUtility::trimExplode(' // ', $parts[1] ?? '', true);
                }
            }
        }
        return new self($rules);
    }

    /**
     * Evaluates properties using the provided array of values.
     *
     * This method evaluates for each mapped property the associated templates
     * by substituting references (e.g. <cn>) with their corresponding values.
     * If a reference is not provided, then the value one is taken. The mapped
     * property is assigned the first nontrivial string.
     *
     * @param array $values
     * @return array
     */
    public function eval(array $values): array
    {
        $result = [];
        foreach ($this->rules as $property => $templates) {
            foreach ($templates as $template) {
                $value = self::evaluateTemplate($template, $values);
                if ($value) {
                    $result[$property] = $value;
                    break;
                }
            }
        }
        return $result;
    }

    /**
     * Replaces all claim references (e.g. <cn>) in the template string with their
     * corresponding values from the provided array. If a claim is not provided,
     * then the empty value is taken.
     *
     * @param string $template  The template string to be evaluated
     * @param array $values     The list of claims for substitutions
     * @return string    The evaluated template
     */
    static public function evaluateTemplate(string $template, array $values): string
    {
        // Must use strtolower, because ldap extension makes all entries lowercase...
        return preg_replace_callback(
            '/<(.+?)>/',
            function ($matches) use($values) { return $values[strtolower($matches[1])] ?? ''; },
            $template
        );
    }
}