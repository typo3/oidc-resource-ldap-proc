<?php
declare(strict_types=1);

namespace IMATHUZH\OidcResourceLdapProcessor\Utility;

/**
 * A list of constants related to this extension.
 */
class Constants
{
    /***********************************/
    /*      Extension constants        */
    /***********************************/

    /** The extension key */
    const EXT_KEY = 'oidc_resource_ldap_proc';

    /** The extension name: the key in PascalCase */
    const EXT_NAME = 'OidcResourceLdapProc';

    /** The extension prefix: the key without underscores */
    const PREFIX = 'oidcresourceldapproc';

    /** The module name: the extension name prefixed with 'tx_' */
    const MOD_NAME = 'tx_oidcresourceldapproc';


    const PROCESSOR_ID = 'ldap-sync';
}