<?php
declare(strict_types=1);

(static function() {

    $procId = \IMATHUZH\OidcResourceLdapProcessor\Utility\Constants::PROCESSOR_ID;
    $extkey = \IMATHUZH\OidcResourceLdapProcessor\Utility\Constants::EXT_KEY;

    \IMATHUZH\OidcClient\Utility\PluginManager::addResourceProcessor(
        $procId,
        "FILE:EXT:$extkey/Configuration/FlexForms/ResourceProcessor.xml"
    );

})();
